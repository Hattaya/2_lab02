module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }

        return true;
    },

    isAgeValid: function (age) {
        if (isNaN(age)) {
            return false;
        }
        if (Number(age) < 18 || Number(age) > 100) {
            return false;
        }
        return true;
    },

    isUppercase: function (password) {

    },

    isPasswordValid: function (password) {
        if (password.length < 8) {
            return false;
        }
        const ans = /[A-Z]/
        if (!ans.test(password)) {
            return false;
        }
        const num = /(?=.*\d{3})/
        if(!num.test(password)){
            return false;
        }
        const sp = /(?=.*\W)/
        if(!sp.test(password)){
            return false;
        }

        return true;
    },

    isDateValid: function (day, month, year) {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            if (day < 1 || day > 31) {
                return false;
            }
        }
        if (month == 2) {
            if (year % 400 !== 0) {
                if (day < 1 || day > 28) {
                    return false;
                }
            }
        } else {
            if (day < 1 || day > 30) {
                return false;
            }
        }
        if (month < 1 || month > 12) {
            return false;
        }
        if (year < 1970 || year > 2020) {
            return false;
        }
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {

        }

        return true;
    }

}